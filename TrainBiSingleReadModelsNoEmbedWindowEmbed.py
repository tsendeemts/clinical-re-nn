import math
import sys
import time
import copy
import pandas as pd
import numpy as np
import itertools
import six
from collections import Counter
from os.path import join
from os import walk
from optparse import OptionParser
import six.moves.cPickle as pickle

from chainer import cuda, Variable, FunctionSet, optimizers
import chainer.functions  as F

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.utils import resample

from Preprocessing import preprocess, fill_batch2, batch, fill_con_batch2, filterl1, preprocess3, all_vocab, bucket_len, prepare_batch_bucket, filterl2
from Preprocessing import split_dataset, flatten, fill_con_batch2_b
from BiLSTMSingleReadConcatModel import BiLSTMSingleReadConcatModel
from BiLSTAtMSingleReadConcatModelEmbed import BiLSTAtMSingleReadConcatModelEmbed

parser = OptionParser()
parser.add_option("-g", "--gpu", dest="gpu",
				  help="gpu to use")
parser.add_option("-w", "--win", dest="window",
				  help="window to consdier")
parser.add_option("-s", "--neg", dest="downsample",
				  help="negative downsample")

(options, args) = parser.parse_args()

w_s = int(options.window)
sub_sample = float(options.downsample)
gpu = int(options.gpu)
print "window size:", w_s
print "sample size:", sub_sample


OOV = 0

def get_vec(word):
	global OOV
	try:
		vec = vectors[word]
	except:
		vec = np.zeros((1, 300), dtype=np.float32)
		OOV += 1
	return vec

def word2vec(docs):
	global OOV
	OOV = 0
	docs = {k:[get_vec(word.strip()) for word in v.split(" ")] for k,v in docs.iteritems()}
	print "OOV:",OOV
	return docs

def downsample(lbls, sub_sample=0.3, neg_lbl="None"):
	neg_indeces = []
	for i in xrange(0, len(lbls)):
		lbl = lbls[i]
		if lbl == neg_lbl:
			neg_indeces.append(i)
	sub = len(neg_indeces) - int(len(neg_indeces)*sub_sample)
	remove_sub = resample(neg_indeces, replace = False, n_samples=sub, random_state=123)
	remove_sub = set(remove_sub)
	return [i for i in xrange(0, len(lbls)) if i not in remove_sub]

def load_docs(dir):
	docs = {}
	fs = []
	for (dirpath, dirnames, filenames) in walk(dir):
		fs.extend(filenames)
		break
	for f in fs:
		with open(join (dir, f)) as thefile:
			lines = thefile.readlines()
			docs[int(f.split(".")[0])] = "".join(lines)
	return docs

def construct_voc(train_docs, min_freq):
	voc_freq = {}
	for doc in train_docs.values():
		words = doc.split(" ")
		for word in words:
			word = word.strip()
			voc_freq[word] = voc_freq.get(word, 0) + 1
	print "voc size:", len(voc_freq)
	return set([k for k,v in voc_freq.iteritems() if v >= min_freq])

def to_id(docs, VOC):
	return {k:[VOC.get(word.strip(), unk_id) for word in v.split(" ")] for k,v in docs.iteritems()}

print "loading lbls..."
train_lbls = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
dev_lbls = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
test_lbls = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
train_lbls = train_lbls.men_lbls
dev_lbls = dev_lbls.men_lbls
test_lbls = test_lbls.men_lbls

print "# train examples:", len(train_lbls)
print "# dev examples:", len(dev_lbls)
print "# test examples:", len(test_lbls)

print "train lbl stats:", Counter(train_lbls.tolist())
print "dev lbl stats:", Counter(dev_lbls.tolist())
print "test lbl stats:", Counter(test_lbls.tolist())

print "downsampling negative examples..."
train_inds = downsample(train_lbls, sub_sample=sub_sample)
train_lbls = train_lbls[train_inds]
# dev_inds = downsample(dev_lbls, sub_sample=0.01)
# dev_lbls = dev_lbls[dev_inds]
print "downsampled train examples:", len(train_lbls)
print "downsampled train lbl stats:", Counter(train_lbls.tolist())
print "downsampled dev examples:", len(dev_lbls)
print "downsampled dev lbl stats:", Counter(dev_lbls.tolist())

train_rels = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)
dev_rels = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)
test_rels = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)

train_docs = load_docs('/home/tsendeemts/pros/records/train/docs/')
dev_docs = load_docs('/home/tsendeemts/pros/records/dev/docs/')
test_docs = load_docs('/home/tsendeemts/pros/records/test/docs/')

train_mentype = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])
dev_mentype = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])
test_mentype = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])

train_l_mentype = train_mentype.l_mentype[train_inds]
dev_l_mentype = dev_mentype.l_mentype
test_l_mentype = test_mentype.l_mentype

train_r_mentype = train_mentype.r_mentype[train_inds]
dev_r_mentype = dev_mentype.r_mentype
test_r_mentype = test_mentype.r_mentype

train_features = []
dev_features = []
test_features = []

mens_str = [u'drugname', u'other', u'indication', u'severity', u'frequency',
       u'route', u'dosage', u'adverse+effect', u'duration']

def mentype2ind(ds):
	for ind, typ in enumerate(mens_str):
		ds[ds == typ] = ind
	return ds.astype(np.int32)
	
# Type of left mention being considered
train_features.append(mentype2ind(train_l_mentype))
dev_features.append(mentype2ind(dev_l_mentype))
test_features.append(mentype2ind(test_l_mentype))

# Type of right mention being considered
train_features.append(mentype2ind(train_r_mentype))
dev_features.append(mentype2ind(dev_r_mentype))
test_features.append(mentype2ind(test_r_mentype))

train_feat = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
dev_feat = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
test_feat = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)

# token distance between two mentions
train_tokendist = train_feat.tokendist[train_inds].reshape((-1))
dev_tokendist = dev_feat.tokendist.reshape((-1))
test_tokendist = test_feat.tokendist.reshape((-1))
train_tokendist[train_tokendist > 300] = 300
dev_tokendist[dev_tokendist > 300] = 300
test_tokendist[test_tokendist > 300] = 300

train_features.append(train_tokendist)
dev_features.append(dev_tokendist)
test_features.append(test_tokendist)

# mention distance between two mentions (how many mentions in between)
train_mendist = train_feat.mendist[train_inds].reshape((-1))
dev_mendist = dev_feat.mendist.reshape((-1))
test_mendist = test_feat.mendist.reshape((-1))
train_features.append(train_mendist)
dev_features.append(dev_mendist)
test_features.append(test_mendist)

mentype_voc = len(np.unique(train_features[0]))
tokendist_voc = 301
mendist_voc = len(np.unique(train_features[3]))

print "mentype_voc:",mentype_voc
print "tokendist_voc:",tokendist_voc
print "mendist_voc:",mendist_voc

print "preprocessing..."
VOC = construct_voc(train_docs, 1) | construct_voc(dev_docs, 1) | construct_voc(test_docs, 1)
with open('/home/tsendeemts/pros/glove.840B.300d.txt', 'r') as f:
		vectors = {}
		for line in f:
			vals = line.rstrip().split(' ')
			if vals[0] in VOC:
				vectors[vals[0]] =  np.array(vals[1:], ndmin=2, dtype=np.float32)
		print "total vocs:", len(VOC)
		print "# of vecs:", len(vectors)

train_docs = word2vec(train_docs)
dev_docs = word2vec(dev_docs)
test_docs = word2vec(test_docs)

w_s = w_s/2

train = [[train_docs[docid][max(l-w_s, 0):l],train_docs[docid][max(r-w_s, 0):r]] for docid,l,r in zip(train_rels.docids[train_inds], train_rels.leftmen[train_inds], train_rels.rightmen[train_inds])]
dev = [[dev_docs[docid][max(l-w_s, 0):l],dev_docs[docid][max(r-w_s, 0):r]] for docid,l,r in zip(dev_rels.docids, dev_rels.leftmen, dev_rels.rightmen)]
test = [[test_docs[docid][max(l-w_s, 0):l],test_docs[docid][max(r-w_s, 0):r]] for docid,l,r in zip(test_rels.docids, test_rels.leftmen, test_rels.rightmen)]

train_b = [[train_docs[docid][l-1:l+w_s-1],train_docs[docid][r-1:r+w_s-1]] for docid,l,r in zip(train_rels.docids[train_inds], train_rels.leftmen[train_inds], train_rels.rightmen[train_inds])]
dev_b = [[dev_docs[docid][l-1:l+w_s-1],dev_docs[docid][r-1:r+w_s-1]] for docid,l,r in zip(dev_rels.docids, dev_rels.leftmen, dev_rels.rightmen)]
test_b = [[test_docs[docid][l-1:l+w_s-1],test_docs[docid][r-1:r+w_s-1]] for docid,l,r in zip(test_rels.docids, test_rels.leftmen, test_rels.rightmen)]

le = LabelEncoder()
le.fit(train_lbls)
train_lbls = np.array(le.transform(train_lbls))
dev_lbls = np.array(le.transform(dev_lbls))
test_lbls = np.array(le.transform(test_lbls))

n_epoch   = 60   # number of epochs
n_units   = 100  # number of units per layer
batch_size = 2048 # minibatch size
eval_batch = 5000
n_output = le.classes_.shape[0]
n_voc = len(VOC)
print "n_output:", n_output
n_train = len(train)
n_dev = len(dev)
n_test = len(test)

print "n_train:",n_train
print "n_dev:",n_dev
print "n_test:",n_test

print "feature shapes:"
for ds in [train_features, dev_features, test_features]:
	print "=========="
	for d in ds:
		print d.shape

model_path = 'models/BiLSTAtMSingleReadConcatModelEmbed_w' + str(w_s) + '_s' + str(sub_sample) + '.'
pad = np.random.uniform(-0.1, 0.1, (1, 300)).astype(np.float32)
pickle.dump(pad, open(model_path + 'pad', 'wb'), -1)

maxes = []
print "building model..."
model = BiLSTAtMSingleReadConcatModelEmbed(n_output, n_units, gpu, mentype_voc, tokendist_voc, mendist_voc)
model.init_optimizer()
best_f1 = 0
best_epoch = 0
print "Train looping..."
for i in xrange(0, n_epoch):
	print "epoch={}".format(i)
	#Shuffle the data
	shuffle = np.random.permutation(n_train)
	preds=[]
	preds_true=[]
	begin_time = time.time()
	for j in six.moves.range(0, n_train, batch_size):
		c_b = shuffle[j:min(j+batch_size, n_train)]
		ys = train_lbls[c_b]
		preds_true.extend(ys)
		y_data = np.array(ys, dtype=np.int32)
		sent_batch_f = batch(train, c_b)
		sent_batch_f, n_steps = fill_con_batch2(sent_batch_f, pad)
		sent_batch_b = batch(train_b, c_b)
		sent_batch_b, n_steps = fill_con_batch2_b(sent_batch_b, pad)
		mentype_l = train_features[0][c_b]
		mentype_r = train_features[1][c_b]
		tokendist = train_features[2][c_b]
		mendist = train_features[3][c_b]
		y_s, loss = model.train(sent_batch_f, sent_batch_b, n_steps, mentype_l, mentype_r, tokendist, mendist, y_data)
		preds.extend(y_s)
	now = time.time()
	duration = now - begin_time
	print 'secs per train epoch={}'.format(duration)
	print 'train f1_score={}'.format(f1_score(preds_true, preds, average='weighted'))
	print classification_report(preds_true, preds)
	print confusion_matrix(preds_true, preds)
	preds=[]
	for j in six.moves.range(0, n_dev, eval_batch):
		sent_batch_f = dev[j:j+eval_batch]
		sent_batch_f, n_steps = fill_con_batch2(sent_batch_f, pad)
		sent_batch_b = dev_b[j:j+eval_batch]
		sent_batch_b, n_steps = fill_con_batch2_b(sent_batch_b, pad)
		mentype_l = dev_features[0][j:j+eval_batch]
		mentype_r = dev_features[1][j:j+eval_batch]
		tokendist = dev_features[2][j:j+eval_batch]
		mendist = dev_features[3][j:j+eval_batch]
		y_s = model.predict(sent_batch_f, sent_batch_b, n_steps, mentype_l, mentype_r, tokendist, mendist)
		preds.extend(y_s)
	f1 = f1_score(dev_lbls, preds, average='weighted')
	print 'dev f1_score={}'.format(f1)
	print classification_report(dev_lbls, preds)
	print confusion_matrix(dev_lbls, preds)
	print 'saving model...'
	model.save(model_path + str(i))
	if f1 >= best_f1 and i > 4:
		best_f1 = f1
		best_epoch = i
	print "best f1 so far:", best_f1
	print "best epoch so far:", best_epoch
print "====================================================================================="
print "testing model the best model..."
print "best f1-score:", best_f1
print "best epoch:", best_epoch
model = BiLSTAtMSingleReadConcatModelEmbed.load(model_path + str(16), n_output, n_units, gpu, mentype_voc, tokendist_voc, mendist_voc)
preds=[]
for j in six.moves.range(0, n_test, eval_batch):
	sent_batch_f = test[j:j+eval_batch]
	sent_batch_f, n_steps = fill_con_batch2(sent_batch_f, pad)
	sent_batch_b = test_b[j:j+eval_batch]
	sent_batch_b, n_steps = fill_con_batch2_b(sent_batch_b, pad)
	mentype_l = test_features[0][j:j+eval_batch]
	mentype_r = test_features[1][j:j+eval_batch]
	tokendist = test_features[2][j:j+eval_batch]
	mendist = test_features[3][j:j+eval_batch]
	y_s = model.predict(sent_batch_f, sent_batch_b, n_steps, mentype_l, mentype_r, tokendist, mendist)
	preds.extend(y_s)
print 'test f1_score={}'.format(f1_score(test_lbls, preds, average='weighted'))
print classification_report(test_lbls, preds)
print confusion_matrix(test_lbls, preds)