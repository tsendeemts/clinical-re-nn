# README #

This is implementation of baseline methods for clinical relation extraction. Python implementation of three different methods can be found here: co-occurrence, SVM + Features, RNN-LSTMs + Attention.

### How do I get set up? ###

* Neural networks are implemented with chainer: http://chainer.org/
* SVM and other utilities depend on sklearn (and of course scipy and numpy): http://scikit-learn.org