import sys
from time import time
from collections import Counter
from os.path import join
from os import walk

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
import scipy.sparse as sp

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn import pipeline, grid_search
from nltk.stem.porter import *
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction import text
from sklearn.cross_validation import StratifiedShuffleSplit, PredefinedSplit
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import resample
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.grid_search import GridSearchCV

def downsample(lbls, sub_sample=0.3, neg_lbl="None"):
	neg_indeces = []
	for i in xrange(0, len(lbls)):
		lbl = lbls[i]
		if lbl == neg_lbl:
			neg_indeces.append(i)
	sub = len(neg_indeces) - int(len(neg_indeces)*sub_sample)
	remove_sub = resample(neg_indeces, replace = False, n_samples=sub, random_state=123)
	remove_sub = set(remove_sub)
	return [i for i in xrange(0, len(lbls)) if i not in remove_sub]

def load_docs(dir):
	docs = {}
	fs = []
	for (dirpath, dirnames, filenames) in walk(dir):
		fs.extend(filenames)
		break
	for f in fs:
		with open(join (dir, f)) as thefile:
			lines = thefile.readlines()
			docs[int(f.split(".")[0])] = "".join(lines).split(" ")
	return docs

print "loading lbls..."
train_lbls = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
dev_lbls = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
test_lbls = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
train_lbls = train_lbls.men_lbls
dev_lbls = dev_lbls.men_lbls
test_lbls = test_lbls.men_lbls

print "# train examples:", len(train_lbls)
print "# dev examples:", len(dev_lbls)
print "# test examples:", len(test_lbls)

print "train lbl stats:", Counter(train_lbls.tolist())
print "dev lbl stats:", Counter(dev_lbls.tolist())
print "test lbl stats:", Counter(test_lbls.tolist())


print "downsampling negative examples..."
train_inds= downsample(train_lbls, sub_sample=0.3)
train_lbls=train_lbls[train_inds]
print "downsampled train examples:", len(train_lbls)
print "downsampled train lbl stats:", Counter(train_lbls.tolist())

print "extracting features..."
train_features = []
dev_features = []
test_features = []

train_types = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['men_types'], encoding='utf-8', usecols=[2])
dev_types = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['men_types'], encoding='utf-8', usecols=[2])
test_types = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['men_types'], encoding='utf-8', usecols=[2])

train_types = train_types.men_types[train_inds]
dev_types = dev_types.men_types
test_types = test_types.men_types

# number of mentions for each type 
cnt_vec = CountVectorizer()
cnt_vec.fit(train_types)
train_features.append(cnt_vec.transform(train_types))
dev_features.append(cnt_vec.transform(dev_types))
test_features.append(cnt_vec.transform(test_types))

train_mentype = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])
dev_mentype = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])
test_mentype = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['l_mentype', 'r_mentype'], encoding='utf-8', usecols=[5,6])

train_l_mentype = train_mentype.l_mentype[train_inds]
dev_l_mentype = dev_mentype.l_mentype
test_l_mentype = test_mentype.l_mentype

train_r_mentype = train_mentype.r_mentype[train_inds]
dev_r_mentype = dev_mentype.r_mentype
test_r_mentype = test_mentype.r_mentype

# Type of left mention being considered
cnt_vec = CountVectorizer()
cnt_vec.fit(train_l_mentype)
train_features.append(cnt_vec.transform(train_l_mentype))
dev_features.append(cnt_vec.transform(dev_l_mentype))
test_features.append(cnt_vec.transform(test_l_mentype))

# Type of right mention being considered
cnt_vec = CountVectorizer()
cnt_vec.fit(train_r_mentype)
train_features.append(cnt_vec.transform(train_r_mentype))
dev_features.append(cnt_vec.transform(dev_r_mentype))
test_features.append(cnt_vec.transform(test_r_mentype))

train_feat = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
dev_feat = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
test_feat = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)

# number of mentions in particular doc
train_nmens = csr_matrix(train_feat.nmens[train_inds].reshape((-1, 1)))
dev_nmens = csr_matrix(dev_feat.nmens.reshape((-1, 1)))
test_nmens = csr_matrix(test_feat.nmens.reshape((-1, 1)))
train_features.append(train_nmens)
dev_features.append(dev_nmens)
test_features.append(test_nmens)

# token distance between two mentions
train_tokendist = csr_matrix(train_feat.tokendist[train_inds].reshape((-1, 1)))
dev_tokendist = csr_matrix(dev_feat.tokendist.reshape((-1, 1)))
test_tokendist = csr_matrix(test_feat.tokendist.reshape((-1, 1)))
train_features.append(train_tokendist)
dev_features.append(dev_tokendist)
test_features.append(test_tokendist)

test_tokendist_dense = test_feat.tokendist.reshape((-1, 1))

# mention distance between two mentions (how many mentions in between)
train_mendist = csr_matrix(train_feat.mendist[train_inds].reshape((-1, 1)))
dev_mendist = csr_matrix(dev_feat.mendist.reshape((-1, 1)))
test_mendist = csr_matrix(test_feat.mendist.reshape((-1, 1)))
train_features.append(train_mendist)
dev_features.append(dev_mendist)
test_features.append(test_mendist)

train_rels = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)
dev_rels = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)
test_rels = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['docids','leftmen','rightmen'], encoding='utf-8', usecols=[0,7,8], dtype=np.int32)

train_docs = load_docs('/home/tsendeemts/pros/records/train/docs/')
dev_docs = load_docs('/home/tsendeemts/pros/records/dev/docs/')
test_docs = load_docs('/home/tsendeemts/pros/records/test/docs/')

l_mens = []
r_mens = []
b_tokens = []
l_tokens = []
r_tokens = []
for i in train_inds:
	docid = train_rels.docids[i]
	l_men = train_rels.leftmen[i]
	r_men = train_rels.rightmen[i]
	l_mens.append(train_docs[docid][l_men])
	r_mens.append(train_docs[docid][r_men])
	b_tokens.append(" ".join(train_docs[docid][min(l_men, r_men):max(l_men, r_men)]))
	l_tokens.append(" ".join(train_docs[docid][max(0, min(l_men, r_men)-4):min(l_men, r_men)]))
	r_tokens.append(" ".join(train_docs[docid][max(l_men, r_men):max(l_men, r_men)+4]))

# left mention char n-grams
cnt_vec_l_men = TfidfVectorizer(analyzer='char', ngram_range=(2, 3))
cnt_vec_l_men.fit(l_mens)
train_features.append(cnt_vec_l_men.transform(l_mens))

# right mention char n-grams
cnt_vec_r_men = TfidfVectorizer(analyzer='char', ngram_range=(2, 3))
cnt_vec_r_men.fit(r_mens)
train_features.append(cnt_vec_r_men.transform(r_mens))

# n-grams of tokens between mentions
cnt_vec_b_tokens = TfidfVectorizer(ngram_range=(1, 3))
cnt_vec_b_tokens.fit(b_tokens)
train_features.append(cnt_vec_b_tokens.transform(b_tokens))

# n-grams of left tokens of the left most mention
cnt_vec_l_tokens = TfidfVectorizer(ngram_range=(1, 3))
cnt_vec_l_tokens.fit(l_tokens)
train_features.append(cnt_vec_l_tokens.transform(l_tokens))

# n-grams of right tokens of the right most mention
cnt_vec_r_tokens = TfidfVectorizer(ngram_range=(1, 3))
cnt_vec_r_tokens.fit(r_tokens)
train_features.append(cnt_vec_r_tokens.transform(r_tokens))

# do the same feature extraction for dev and test using the trained vectorizers
for features, dataset, docs in zip([dev_features, test_features],[dev_rels, test_rels], [dev_docs, test_docs]):
	l_mens = []
	r_mens = []
	b_tokens = []
	l_tokens = []
	r_tokens = []
	for i in xrange(len(dataset.docids)):
		docid = dataset.docids[i]
		l_men = dataset.leftmen[i]
		r_men = dataset.rightmen[i]
		l_mens.append(docs[docid][l_men])
		r_mens.append(docs[docid][r_men])
		b_tokens.append(" ".join(docs[docid][min(l_men, r_men):max(l_men, r_men)]))
		l_tokens.append(" ".join(docs[docid][max(0, min(l_men, r_men)-4):min(l_men, r_men)]))
		r_tokens.append(" ".join(docs[docid][max(l_men, r_men):max(l_men, r_men)+4]))
	
	features.append(cnt_vec_l_men.transform(l_mens))
	features.append(cnt_vec_r_men.transform(r_mens))
	features.append(cnt_vec_b_tokens.transform(b_tokens))
	features.append(cnt_vec_l_tokens.transform(l_tokens))
	features.append(cnt_vec_r_tokens.transform(r_tokens))

train_features = sp.hstack(train_features, format='csr')
dev_features = sp.hstack(dev_features, format='csr')
test_features = sp.hstack(test_features, format='csr')

train_features = sp.vstack((train_features, dev_features), format='csr')
train_lbls = np.concatenate((train_lbls, dev_lbls), axis=0)
folds = np.zeros_like(train_lbls)
folds[:train_lbls.shape[0]-dev_lbls.shape[0]] = -1
ps = PredefinedSplit(folds)
parameters = {
	'C': (10.0 ** np.arange(-4, 4)).tolist(),
	'dual': [True, False],
	'tol': (10.0 ** np.arange(-8, 2)).tolist(),
	'class_weight': [None, 'auto']}

print "training model..."
clf = GridSearchCV(LinearSVC(), param_grid=parameters, cv=ps, verbose=2, scoring='f1_weighted', n_jobs=8)
clf.fit(train_features, train_lbls)

# clf = LinearSVC(C=100.0, dual=True, tol=0.1, class_weight=None)
# clf.fit(train_features, train_lbls)
preds = clf.predict(train_features)
print 'train f1_score={}'.format(f1_score(train_lbls, preds, average='weighted'))
print 'train accuracy_score={}'.format(accuracy_score(train_lbls, preds))
print classification_report(train_lbls, preds)
print confusion_matrix(train_lbls, preds)

preds = clf.predict(dev_features)
print 'dev f1_score={}'.format(f1_score(dev_lbls, preds, average='weighted'))
print 'dev accuracy_score={}'.format(accuracy_score(dev_lbls, preds))
print classification_report(dev_lbls, preds)
print confusion_matrix(dev_lbls, preds)


preds = clf.predict(test_features)
print 'test f1_score={}'.format(f1_score(test_lbls, preds, average='weighted'))
print 'test accuracy_score={}'.format(accuracy_score(test_lbls, preds))
print classification_report(test_lbls, preds)
print confusion_matrix(test_lbls, preds)

print "Best parameters set found on development set:"
print "\n"
print(clf.best_params_)
print "\n"
print "Grid scores on development set:"
print "\n"
for params, mean_score, scores in clf.grid_scores_:
	print ("%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() * 2, params))
print "\n"