import math
import sys
import time
import copy
import pandas as pd
import numpy as np
import six
import six.moves.cPickle as pickle
from chainer import serializers
from chainer import cuda, Variable, FunctionSet, optimizers
import chainer.functions  as F
import chainer.links as L
import chainer

class BiLSTMSingleReadConcatModel(chainer.Chain):

	"""docstring for BiLSTMSingleReadConcatModel"""
	def __init__(self, n_outputs, n_units, gpu):
		super(BiLSTMSingleReadConcatModel, self).__init__(
			w_p = F.Linear(300, n_units),
			f_lstm = L.LSTM(n_units, n_units),
			b_lstm = L.LSTM(n_units, n_units),
			w_c = F.Linear(n_units*4, n_units*4),
			l_y = F.Linear(n_units*4, n_outputs))
		self.__n_units = n_units
		self.__gpu = gpu
		self.__mod = cuda.cupy if gpu >= 0 else np
		for param in self.params():
			data = param.data
			data[:] = np.random.uniform(-0.1, 0.1, data.shape)
		if gpu >= 0:
			cuda.get_device(gpu).use()
			self.to_gpu()

	def save(self, filename):
		serializers.save_hdf5(filename, self)

	@staticmethod
	def load(filename, n_outputs, n_units, gpu):
		self = BiLSTMSingleReadConcatModel(n_outputs, n_units, gpu)
		serializers.load_hdf5(filename, self)
		self.__gpu = gpu
		self.__mod = cuda.cupy if gpu >= 0 else np
		self.__n_units = n_units
		self.__n_output = n_outputs
		if gpu >= 0:
			cuda.get_device(gpu).use()
			self.to_gpu()
		return self

	def reset_state(self):
		self.f_lstm.reset_state()
		self.b_lstm.reset_state()

	def init_optimizer(self):
		self.__opt = optimizers.Adam(alpha=0.001, beta1=0.9, beta2=0.999, eps=1e-08)
		# self.__opt = optimizers.SGD(lr=.1)
		self.__opt.setup(self)
		self.__opt.add_hook(chainer.optimizer.GradientClipping(20))
		self.__opt.add_hook(chainer.optimizer.WeightDecay(0.0001))

	def __forward(self, train, x_batch_f, x_batch_b, n_step, y_batch = None):
		model = self
		n_units = self.__n_units
		mod = self.__mod
		gpu = self.__gpu
		batch_size = len(x_batch_f)
		x_len = len(x_batch_f[0])

		if gpu >=0:
			x_batch_f = [[cuda.to_gpu(e) for e in row] for row in x_batch_f]
			x_batch_b = [[cuda.to_gpu(e) for e in row] for row in x_batch_b]

		# forward 
		self.reset_state()
		list_a = []
		for l in range(x_len):
			x_data = mod.concatenate([x_batch_f[k][l] for k in range(batch_size)])
			x = Variable(x_data, volatile=not train)
			h1 = model.f_lstm(model.w_p(F.dropout(x, ratio=0.0, train=train)))
			list_a.append(h1)
		
		# backward
		list_b = []
		for l in reversed(range(x_len)):
			x_data = mod.concatenate([x_batch_b[k][l] for k in range(batch_size)])
			x = Variable(x_data, volatile=not train)
			h1 = model.b_lstm(model.w_p(F.dropout(x, ratio=0.0, train=train)))
			list_b.append(h1)
			# list_b.insert(0, h1)

		h2 = F.concat([list_a[n_step-1], list_b[n_step*2-1], list_a[n_step*2-1], list_b[n_step-1]], axis=1)
		h3 = F.tanh(model.w_c(h2))
		y = model.l_y(F.dropout(h3, ratio=0.0, train=train))
		preds = mod.argmax(y.data, 1).tolist()

		accum_loss = Variable(mod.zeros(()), volatile=not train) if train else None
		if train:
			if self.__gpu >= 0:
				y_batch = cuda.to_gpu(y_batch)
			lbl = Variable(y_batch, volatile=not train)
			accum_loss = F.softmax_cross_entropy(y, lbl)
		
		return preds, accum_loss

	def train(self, x_batch_f, x_batch_b, n_step, y_batch):
		self.__opt.zero_grads()
		preds, accum_loss = self.__forward(True, x_batch_f, x_batch_b, n_step, y_batch=y_batch)
		accum_loss.backward()
		self.__opt.update()
		return preds, accum_loss

	def predict(self, x_batch_f, x_batch_b, n_step):
		return self.__forward(False, x_batch_f, x_batch_b, n_step)[0]