import sys
from time import time
from collections import Counter
from os.path import join
from os import walk
import operator
from random import randint

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
import scipy.sparse as sp

from Preprocessing import batch

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn import pipeline, grid_search
from nltk.stem.porter import *
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction import text
from sklearn.cross_validation import StratifiedShuffleSplit, PredefinedSplit
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import resample
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.grid_search import GridSearchCV

def downsample(lbls, sub_sample=0.3, neg_lbl="None"):
	neg_indeces = []
	for i in xrange(0, len(lbls)):
		lbl = lbls[i]
		if lbl == neg_lbl:
			neg_indeces.append(i)
	sub = len(neg_indeces) - int(len(neg_indeces)*sub_sample)
	remove_sub = resample(neg_indeces, replace = False, n_samples=sub, random_state=123)
	remove_sub = set(remove_sub)
	return [i for i in xrange(0, len(lbls)) if i not in remove_sub]

def load_docs(dir):
	docs = {}
	fs = []
	for (dirpath, dirnames, filenames) in walk(dir):
		fs.extend(filenames)
		break
	for f in fs:
		with open(join (dir, f)) as thefile:
			lines = thefile.readlines()
			docs[int(f.split(".")[0])] = "".join(lines).split(" ")
	return docs

print "loading lbls..."
train_lbls = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
dev_lbls = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
test_lbls = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['men_lbls'], encoding='utf-8', usecols=[9])
train_lbls = train_lbls.men_lbls
dev_lbls = dev_lbls.men_lbls
test_lbls = test_lbls.men_lbls

print "# train examples:", len(train_lbls)
print "# dev examples:", len(dev_lbls)
print "# test examples:", len(test_lbls)

print "train lbl stats:", Counter(train_lbls.tolist())
print "dev lbl stats:", Counter(dev_lbls.tolist())
print "test lbl stats:", Counter(test_lbls.tolist())

train_feat = pd.read_csv("/home/tsendeemts/pros/records/train/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
dev_feat = pd.read_csv("/home/tsendeemts/pros/records/dev/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)
test_feat = pd.read_csv("/home/tsendeemts/pros/records/test/relations.csv", header=None, names=['nmens','tokendist','mendist'], encoding='utf-8', usecols=[1,3,4], dtype=np.int32)

# token distance between two mentions
train_tokendist = train_feat.tokendist
dev_tokendist = dev_feat.tokendist
test_tokendist = test_feat.tokendist

def dist_stats():
	avgs = {}
	for lbl in train_lbls.unique():
		print "Label:",lbl
		inds = train_lbls == lbl
		tokendist = train_tokendist[inds]
		avg = np.average(tokendist)
		print "avg:",avg
		print "max:",np.max(tokendist)
		avgs[lbl] = avg

	print sorted(avgs.items(), key=operator.itemgetter(1))

dist_map = {u'do': 2.5622398789254635,
u'severity_type': 2.959468438538206,
u'manner%2Froute': 4.0256813417190775,
u'fr': 6.5982905982905979,
u'du': 7.2535496957403653,
u'adverse': 14.242677824267782,
u'reason': 19.313342025206431,
u'None': 370.59177554421973}

def predict(dist):
	if dist < dist_map['do']:
		return 'do'
	elif dist < dist_map['severity_type']:
		return 'severity_type'
	elif dist < dist_map['manner%2Froute']:
		return 'manner%2Froute'
	elif dist < dist_map['fr']:
		return 'fr'
	elif dist < dist_map['du']:
		return 'du'
	elif dist < dist_map['adverse']:
		return 'adverse'
	elif dist < dist_map['reason']:
		return 'reason'
	else:
		return 'None'

pos_lbls = [u'do',
 u'fr',
 u'manner%2Froute',
 u'adverse',
 u'reason',
 u'du',
 u'severity_type']

def predict_random(dist):
	if dist < 8:
		ind = randint(1,len(pos_lbls)) - 1
		return pos_lbls[ind]
	else:
		return 'None'

print "=========================================================================="
print "Heuristic positive lbl selection"
preds = []
for dist in train_tokendist:
	preds.append(predict(dist))
print 'train f1_score={}'.format(f1_score(train_lbls, preds, average='weighted'))
print 'train accuracy_score={}'.format(accuracy_score(train_lbls, preds))
print classification_report(train_lbls, preds)
print confusion_matrix(train_lbls, preds)

preds = []
for dist in dev_tokendist:
	preds.append(predict(dist))
print 'dev f1_score={}'.format(f1_score(dev_lbls, preds, average='weighted'))
print 'dev accuracy_score={}'.format(accuracy_score(dev_lbls, preds))
print classification_report(dev_lbls, preds)
print confusion_matrix(dev_lbls, preds)

preds = []
for dist in test_tokendist:
	preds.append(predict(dist))
print 'test f1_score={}'.format(f1_score(test_lbls, preds, average='weighted'))
print 'test accuracy_score={}'.format(accuracy_score(test_lbls, preds))
print classification_report(test_lbls, preds)
print confusion_matrix(test_lbls, preds)


print "=========================================================================="
print "Random positive lbl selection"

preds = []
for dist in train_tokendist:
	preds.append(predict_random(dist))
print 'train f1_score={}'.format(f1_score(train_lbls, preds, average='weighted'))
print 'train accuracy_score={}'.format(accuracy_score(train_lbls, preds))
print classification_report(train_lbls, preds)
print confusion_matrix(train_lbls, preds)

preds = []
for dist in dev_tokendist:
	preds.append(predict_random(dist))
print 'dev f1_score={}'.format(f1_score(dev_lbls, preds, average='weighted'))
print 'dev accuracy_score={}'.format(accuracy_score(dev_lbls, preds))
print classification_report(dev_lbls, preds)
print confusion_matrix(dev_lbls, preds)

preds = []
for dist in test_tokendist:
	preds.append(predict_random(dist))
print 'test f1_score={}'.format(f1_score(test_lbls, preds, average='weighted'))
print 'test accuracy_score={}'.format(accuracy_score(test_lbls, preds))
print classification_report(test_lbls, preds)
print confusion_matrix(test_lbls, preds)

# Label: reason
# avg: 19.3133420252
# max: 518
# Label: None
# avg: 370.591775544
# max: 2817
# Label: do
# avg: 2.56223987893
# max: 152
# Label: severity_type
# avg: 2.95946843854
# max: 362
# Label: fr
# avg: 6.59829059829
# max: 155
# Label: manner%2Froute
# avg: 4.02568134172
# max: 154
# Label: du
# avg: 7.25354969574
# max: 58
# Label: adverse
# avg: 14.2426778243
# max: 769
# [(u'do', 2.5622398789254635), (u'severity_type', 2.959468438538206), (u'manner%2Froute', 4.0256813417190775), (u'fr', 6.5982905982905979), (u'du', 7.2535496957403653), (u'adverse', 14.242677824267782), (u'reason', 19.313342025206431), (u'None', 370.59177554421973)]

