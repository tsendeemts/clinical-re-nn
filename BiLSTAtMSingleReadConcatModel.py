import math
import sys
import time
import copy
import pandas as pd
import numpy as np
import six
import six.moves.cPickle as pickle
from chainer import serializers
from chainer import cuda, Variable, FunctionSet, optimizers
import chainer.functions  as F
import chainer.links as L
import chainer

class BiLSTAtMSingleReadConcatModel(chainer.Chain):

	"""docstring for BiLSTAtMSingleReadConcatModel"""
	def __init__(self, n_outputs, n_units, gpu):
		super(BiLSTAtMSingleReadConcatModel, self).__init__(
			w_p = F.Linear(300, n_units),
			f_lstm = L.LSTM(n_units, n_units),
			b_lstm = L.LSTM(n_units, n_units),
			w_ap = F.Linear(n_units, n_units),
			w_aw = F.Linear(n_units, n_units),
			w_we = F.Linear(n_units, 1),
			w_lp = F.Linear(n_units, n_units),
			w_lw = F.Linear(n_units, n_units),
			b_w_ap = F.Linear(n_units, n_units),
			b_w_aw = F.Linear(n_units, n_units),
			b_w_we = F.Linear(n_units, 1),
			b_w_lp = F.Linear(n_units, n_units),
			b_w_lw = F.Linear(n_units, n_units),
			w_c = F.Linear(n_units*4, n_units*4),
			l_y = F.Linear(n_units*4, n_outputs))
		self.__n_units = n_units
		self.__gpu = gpu
		self.__mod = cuda.cupy if gpu >= 0 else np
		for param in self.params():
			data = param.data
			data[:] = np.random.uniform(-0.1, 0.1, data.shape)
		if gpu >= 0:
			cuda.get_device(gpu).use()
			self.to_gpu()

	def save(self, filename):
		serializers.save_hdf5(filename, self)

	@staticmethod
	def load(filename, n_outputs, n_units, gpu):
		self = BiLSTAtMSingleReadConcatModel(n_outputs, n_units, gpu)
		serializers.load_hdf5(filename, self)
		self.__gpu = gpu
		self.__mod = cuda.cupy if gpu >= 0 else np
		self.__n_units = n_units
		self.__n_output = n_outputs
		if gpu >= 0:
			cuda.get_device(gpu).use()
			self.to_gpu()
		return self

	def reset_state(self):
		self.f_lstm.reset_state()
		self.b_lstm.reset_state()

	def init_optimizer(self):
		self.__opt = optimizers.Adam(alpha=0.001, beta1=0.9, beta2=0.999, eps=1e-08)
		# self.__opt = optimizers.SGD(lr=.1)
		self.__opt.setup(self)
		self.__opt.add_hook(chainer.optimizer.GradientClipping(20))
		self.__opt.add_hook(chainer.optimizer.WeightDecay(0.0001))

	def __attend_f(self, hs, batch_size, train):
		n_units = self.__n_units
		mod = self.__mod
		
		# calculate attention weights
		x_len = len(hs)-1
		s_p = self.w_ap(hs[-1])
		list_e = []
		sum_e = Variable(mod.zeros((batch_size, 1), dtype=np.float32), volatile=not train)
		for l in range(x_len):
			s_w = self.w_aw(hs[l]) + s_p
			r_e = F.exp(self.w_we(s_w))
			list_e.append(r_e)
			sum_e += r_e

		# make attention vector
		s_c = Variable(mod.zeros((batch_size, n_units), dtype=np.float32), volatile=not train)
		for l in range(x_len):
			s_e = list_e[l] / sum_e
			s_c += F.reshape(F.batch_matmul(hs[l], s_e), (batch_size, n_units))

		l_h = self.w_lp(s_c) + self.w_lw(hs[-1])
		return l_h

	def __attend_b(self, hs, batch_size, train):
		n_units = self.__n_units
		mod = self.__mod
		
		# calculate attention weights
		x_len = len(hs)-1
		s_p = self.b_w_ap(hs[-1])
		list_e = []
		sum_e = Variable(mod.zeros((batch_size, 1), dtype=np.float32), volatile=not train)
		for l in range(x_len):
			s_w = self.b_w_aw(hs[l]) + s_p
			r_e = F.exp(self.b_w_we(s_w))
			list_e.append(r_e)
			sum_e += r_e

		# make attention vector
		s_c = Variable(mod.zeros((batch_size, n_units), dtype=np.float32), volatile=not train)
		for l in range(x_len):
			s_e = list_e[l] / sum_e
			s_c += F.reshape(F.batch_matmul(hs[l], s_e), (batch_size, n_units))

		l_h = self.b_w_lp(s_c) + self.b_w_lw(hs[-1])
		return l_h

	def __forward(self, train, x_batch_f, x_batch_b, n_step, y_batch = None):
		n_units = self.__n_units
		mod = self.__mod
		gpu = self.__gpu
		batch_size = len(x_batch_f)
		x_len = len(x_batch_f[0])

		if gpu >=0:
			x_batch_f = [[cuda.to_gpu(e) for e in row] for row in x_batch_f]
			x_batch_b = [[cuda.to_gpu(e) for e in row] for row in x_batch_b]

		# forward 
		self.reset_state()
		list_a = []
		for l in range(x_len):
			x_data = mod.concatenate([x_batch_f[k][l] for k in range(batch_size)])
			x = Variable(x_data, volatile=not train)
			h1 = self.f_lstm(self.w_p(F.dropout(x, ratio=0.0, train=train)))
			list_a.append(h1)
		
		# backward
		list_b = []
		for l in reversed(range(x_len)):
			x_data = mod.concatenate([x_batch_b[k][l] for k in range(batch_size)])
			x = Variable(x_data, volatile=not train)
			h1 = self.b_lstm(self.w_p(F.dropout(x, ratio=0.0, train=train)))
			list_b.append(h1)

		f_l_a = self.__attend_f(list_a[:n_step], batch_size, train)
		f_r_a = self.__attend_f(list_a[n_step:], batch_size, train)
		b_l_a = self.__attend_b(list_b[n_step:], batch_size, train)
		b_r_a = self.__attend_b(list_b[:n_step], batch_size, train)

		h2 = F.concat([f_l_a, b_l_a, f_r_a, b_r_a], axis=1)
		h3 = F.tanh(self.w_c(h2))
		y = self.l_y(F.dropout(h3, ratio=0.0, train=train))
		preds = mod.argmax(y.data, 1).tolist()

		accum_loss = Variable(mod.zeros(()), volatile=not train) if train else None
		if train:
			if self.__gpu >= 0:
				y_batch = cuda.to_gpu(y_batch)
			lbl = Variable(y_batch, volatile=not train)
			accum_loss = F.softmax_cross_entropy(y, lbl)
		
		return preds, accum_loss

	def train(self, x_batch_f, x_batch_b, n_step, y_batch):
		self.__opt.zero_grads()
		preds, accum_loss = self.__forward(True, x_batch_f, x_batch_b, n_step, y_batch=y_batch)
		accum_loss.backward()
		self.__opt.update()
		return preds, accum_loss

	def predict(self, x_batch_f, x_batch_b, n_step):
		return self.__forward(False, x_batch_f, x_batch_b, n_step)[0]